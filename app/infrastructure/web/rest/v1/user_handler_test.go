package v1

import (
	"errors"
	"github.com/labstack/echo/v4"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/pzentenoe/onboarding/app/domain/models"
	"net/http"
	"net/http/httptest"
	"testing"
)

const (
	responseGetUserByID ="{\"id\":\"123456\",\"name\":\"usuario\",\"password\":\"password\"}\n"
	userNotFoundError ="{\"message\":\"User not found\"}\n"
)

type userUsecaseMock struct {
	mock.Mock
}

func (m *userUsecaseMock) GetUserById(id string) (*models.User, error) {
	mocked := m.Called(id)
	if mocked.Get(0) == nil {
		return nil, mocked.Error(1)
	}
	return mocked.Get(0).(*models.User), mocked.Error(1)
}
func (m *userUsecaseMock) CreateUser(user *models.User) (*models.User, error) {
	mocked := m.Called(user)
	if mocked.Get(0) == nil {
		return nil, mocked.Error(1)
	}
	return mocked.Get(0).(*models.User), mocked.Error(1)
}

func Test_userHandler_getUserById(t *testing.T) {
	t.Parallel()
	t.Run("when user is found return status OK and user", func(t *testing.T) {
		mockUserUsecase := new(userUsecaseMock)
		mockUserUsecase.On("GetUserById", mock.AnythingOfType("string")).Return(createUserMock(), nil)

		e := echo.New()
		req := httptest.NewRequest(http.MethodGet, "/users/123456", nil)
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		rec := httptest.NewRecorder()
		echoContext := e.NewContext(req, rec)
		echoContext.SetParamNames("id")
		echoContext.SetParamValues("123456")

		h := NewUserHandler(e, mockUserUsecase)
		h.getUserById(echoContext)

		assert.Equal(t, http.StatusOK, rec.Code)
		assert.Equal(t, responseGetUserByID, rec.Body.String())

	})

	t.Run("when user not found return error", func(t *testing.T) {
		mockUserUsecase := new(userUsecaseMock)
		mockUserUsecase.On("GetUserById", mock.AnythingOfType("string")).
			Return(nil, errors.New("User not found"))

		e := echo.New()
		req := httptest.NewRequest(http.MethodGet, "/users/123456", nil)
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationJSON)
		rec := httptest.NewRecorder()
		echoContext := e.NewContext(req, rec)
		echoContext.SetParamNames("id")
		echoContext.SetParamValues("123456")

		h := NewUserHandler(e, mockUserUsecase)
		h.getUserById(echoContext)

		assert.Equal(t, http.StatusBadGateway, rec.Code)
		assert.Equal(t, userNotFoundError, rec.Body.String())

	})
}

func createUserMock() *models.User {
	usermodelMock := &models.User{
		ID:       "123456",
		Name:     "usuario",
		Password: "password",
	}
	return usermodelMock
}
