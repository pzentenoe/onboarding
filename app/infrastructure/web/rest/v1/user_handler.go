package v1

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/pzentenoe/onboarding/app/application/usecase"
	"gitlab.com/pzentenoe/onboarding/app/domain/models"
	"net/http"
)

type userHandler struct {
	userUsecase usecase.UserUsecase
}

func NewUserHandler(e *echo.Echo, userUsecase usecase.UserUsecase) *userHandler {
	h := &userHandler{userUsecase: userUsecase}
	g := e.Group("/users")

	g.GET("/:id", h.getUserById)
	g.POST("", h.createUser)
	return h
}

func (h *userHandler) getUserById(c echo.Context) error {
	id := c.Param("id")
	user, err := h.userUsecase.GetUserById(id)
	if err != nil {
		return c.JSON(http.StatusBadGateway, echo.Map{"message": err.Error()})
	}
	return c.JSON(http.StatusOK, user)
}

func (h *userHandler) createUser(c echo.Context) error {

	var user *models.User
	if err := c.Bind(&user); err != nil {
		return c.JSON(http.StatusBadRequest, echo.Map{"message": err.Error()})
	}

	user, err := h.userUsecase.CreateUser(user)
	if err != nil {
		return c.JSON(http.StatusBadGateway, echo.Map{"message": err.Error()})
	}
	return c.JSON(http.StatusCreated, user)
}
