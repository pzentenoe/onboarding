package mapper

import (
	"gitlab.com/pzentenoe/onboarding/app/domain/models"
	"gitlab.com/pzentenoe/onboarding/app/infrastructure/data/db/mongo/entity"
)

func UserEntityToUser(userEntity *entity.User) *models.User {
	user := &models.User{
		ID:       userEntity.ID.Hex(),
		Name:     userEntity.Name,
		Password: userEntity.Password,
	}
	return user
}
