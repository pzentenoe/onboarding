package user

import (
	"go.mongodb.org/mongo-driver/mongo"
)

type userMongoRepository struct {
	client *mongo.Client
}

func NewUserMongoRepository(client *mongo.Client) *userMongoRepository {
	return &userMongoRepository{client: client}
}

func (r *userMongoRepository) getUserCollection() *mongo.Collection {
	collection := r.client.Database("onboarding").Collection("users")
	return collection
}
