package user

import (
	"context"
	"gitlab.com/pzentenoe/onboarding/app/domain/models"
	"gitlab.com/pzentenoe/onboarding/app/infrastructure/data/db/mongo/entity"
	"gitlab.com/pzentenoe/onboarding/app/infrastructure/data/db/mongo/mapper"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"log"
)

func (r *userMongoRepository) FindById(id string) (*models.User, error) {
	collection := r.getUserCollection()

	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		log.Println("Invalid id")
		return nil, err
	}

	filter := bson.M{"_id": oid}

	result := collection.FindOne(context.Background(), filter)
	if result.Err() != nil {
		log.Println("fail to get user")
		return nil, err
	}
	var userEntity *entity.User
	if err := result.Decode(&userEntity); err != nil {
		log.Println("fail decode user")
		return nil, err
	}

	user := mapper.UserEntityToUser(userEntity)

	return user, nil
}


