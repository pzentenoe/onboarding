package user

import (
	"context"
	"errors"
	"gitlab.com/pzentenoe/onboarding/app/domain/models"
	"gitlab.com/pzentenoe/onboarding/app/infrastructure/data/db/mongo/entity"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func (r *userMongoRepository) CreateUser(user *models.User) (*models.User, error) {
	collection := r.getUserCollection()

	userToCreate := entity.User{
		Name:     user.Name,
		Password: user.Password,
	}
	result, err := collection.InsertOne(context.Background(), userToCreate)
	if err != nil {
		return nil, err
	}

	oid, ok := result.InsertedID.(primitive.ObjectID)
	if !ok {
		return nil, errors.New("Failed to convert object id")
	}

	user.ID = oid.Hex()

	return user, nil
}
