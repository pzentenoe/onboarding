package user

import (
	"github.com/stretchr/testify/mock"
	"gitlab.com/pzentenoe/onboarding/app/domain/models"
)

type userRepositoryMock struct {
	mock.Mock
}

func (m *userRepositoryMock) FindById(id string) (*models.User, error) {
	mocked := m.Called(id)
	if mocked.Get(0) == nil {
		return nil, mocked.Error(1)
	}
	return mocked.Get(0).(*models.User), mocked.Error(1)
}

func (m *userRepositoryMock) CreateUser(user *models.User) (*models.User, error) {
	mocked := m.Called(user)
	if mocked.Get(0) == nil {
		return nil, mocked.Error(1)
	}
	return mocked.Get(0).(*models.User), mocked.Error(1)
}
