package user

import "gitlab.com/pzentenoe/onboarding/app/domain/repository"

type userUsecase struct {
	userRepository repository.UserRepository
}

func NewUserUsecase(userRepository repository.UserRepository) *userUsecase {
	return &userUsecase{userRepository: userRepository}
}
