package user

import "gitlab.com/pzentenoe/onboarding/app/domain/models"

func (u *userUsecase) GetUserById(id string) (*models.User, error) {
	return u.userRepository.FindById(id)
}
