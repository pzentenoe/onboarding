package user

import "gitlab.com/pzentenoe/onboarding/app/domain/models"

func (u *userUsecase)CreateUser(user *models.User) (*models.User, error)  {
	return u.userRepository.CreateUser(user)
}
