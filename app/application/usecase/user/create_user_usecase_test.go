package user

import (
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/pzentenoe/onboarding/app/domain/models"
	"testing"
)

func Test_userUsecase_CreateUser(t *testing.T) {
	t.Parallel()
	t.Run("when inserting a new user is successfully return a new user with id", func(t *testing.T) {
		//ARRANGE
		userRepoMock := new(userRepositoryMock)
		userRepoMock.On("CreateUser", mock.AnythingOfType("*models.User")).Return(createUserMock(), nil)
		usecaseUser := NewUserUsecase(userRepoMock)

		userRequest := &models.User{
			Name:     "user",
			Password: "12435",
		}
		//ACT
		user, err := usecaseUser.CreateUser(userRequest)

		//Assert
		assert.NoError(t, err)
		assert.NotNil(t, user)
		assert.Equal(t, "123456", user.ID)
	})
}
