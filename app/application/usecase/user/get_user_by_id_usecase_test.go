package user

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/pzentenoe/onboarding/app/domain/models"
	"testing"
)

func Test_userUsecase_GetUserById(t *testing.T) {

	t.Parallel()
	t.Run("when is user is gotten successfully return a valid user", func(t *testing.T) {

		//Arrange
		userRepoMock := new(userRepositoryMock)
		userRepoMock.On("FindById", mock.AnythingOfType("string")).Return(createUserMock(), nil)
		usecaseUser := NewUserUsecase(userRepoMock)

		//Act
		user, err := usecaseUser.GetUserById("12345")

		//Assert
		assert.NoError(t, err)
		assert.NotNil(t, user)
		assert.Equal(t, "123456", user.ID)

	})
	t.Run("When user id is invalid returns nil user and error", func(t *testing.T) {

		userRepoMock := new(userRepositoryMock)
		userRepoMock.On("FindById", mock.AnythingOfType("string")).
			Return(nil, errors.New("Invalid user id"))
		usecaseUser := NewUserUsecase(userRepoMock)

		user, err := usecaseUser.GetUserById("12345")

		assert.Error(t, err)
		assert.Nil(t, user)
		assert.Equal(t, "Invalid user id", err.Error())

	})

}

func createUserMock() *models.User {
	usermodelMock := &models.User{
		ID:       "123456",
		Name:     "usuario",
		Password: "password",
	}
	return usermodelMock
}



