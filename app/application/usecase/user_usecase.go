package usecase

import "gitlab.com/pzentenoe/onboarding/app/domain/models"

type UserUsecase interface {
	GetUserById(id string) (*models.User, error)
	CreateUser(user *models.User) (*models.User, error)
}
