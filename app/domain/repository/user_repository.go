package repository

import "gitlab.com/pzentenoe/onboarding/app/domain/models"

type UserRepository interface {
	FindById(id string) (*models.User, error)
	CreateUser(user *models.User) (*models.User, error)
}
