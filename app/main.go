package main

import (
	"context"
	"fmt"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	userUsecase "gitlab.com/pzentenoe/onboarding/app/application/usecase/user"
	"gitlab.com/pzentenoe/onboarding/app/infrastructure/data/db/mongo/user"
	v1 "gitlab.com/pzentenoe/onboarding/app/infrastructure/web/rest/v1"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"os"
	"time"
)

func main() {

	e := echo.New()
	e.HideBanner = true
	e.Use(middleware.CORS())

	_ = v1.NewHealthCheck(e)

	fmt.Println(os.Getenv("ONBOARDING_VAR"))

	client := createMongoClient()
	userMongoRepository := user.NewUserMongoRepository(client)
	userUsecase := userUsecase.NewUserUsecase(userMongoRepository)
	_ = v1.NewUserHandler(e, userUsecase)


	e.Logger.Fatal(e.Start(":8080"))
}

func createMongoClient() *mongo.Client {
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		log.Fatal(err.Error())
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err.Error())
	}
	return client
}
