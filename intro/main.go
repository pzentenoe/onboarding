package main

import "fmt"

/*const (
	lunes     = 1
	martes    = 2
	miercoles = 3
	jueves    = 4
	viernes   = 5
)

func main() {

	var letra string
	letra = "a"

	fmt.Println(letra)

	numero := 23
	fmt.Println(numero)

	if numero > 20 {
		fmt.Println(numero)
	}

	var day = lunes

	switch day {

	case lunes, martes, miercoles:
		fmt.Println("Es cyber")
	case jueves:
		fmt.Println("Es jueves")
	}

	for day < jueves {
			fmt.Println("es cyber", day)
			day++
		}

		for {
			fmt.Println("es cyber", day)

			if day==7 {
				break
			}
			day++
		}

		for i:=0; i<10; i++{
		fmt.Println(i)
	}

	//listaDeTerminos:=[2]string{"leche", "manzana",}
	listaDeTerminos := make([]string, 0)
	listaDeTerminos = append(listaDeTerminos, "leche", "manzana", "pera")

	for _, termino := range listaDeTerminos {
		fmt.Println(termino)
	}

	fmt.Println(len(listaDeTerminos))

	numerosMap := make(map[string]int)
	numerosMap["uno"] = 1
	numerosMap["dos"] = 2

	for key, _ := range numerosMap {
		fmt.Println(key)
		//fmt.Println(value)
		fmt.Println(numerosMap[key])
	}

	tres, ok := numerosMap["dos"]

	if ok {
		fmt.Println(tres)
	} else {
		fmt.Println("El numero 3 no fue encontrado")
	}

	if size, page, err := setSizeAndPage("aaa", "aaa"); err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println(size, page)
	}

}

func setSizeAndPage(sizeString, pageString string) (size int, page int, err error) {
	size, err = strconv.Atoi(sizeString)
	if err != nil {
		fmt.Println("Fallo al convertir size")
		return 0, 0, err
	}
	page, err = strconv.Atoi(pageString)
	if err != nil {
		fmt.Println("Fallo al convertir page")
		return 0, 0, err
	}
	return
}*/

func main() {

	var pablo *Persona
	pablo = new(Persona)
	pablo.ID = "1"
	pablo.Name = "Pablo"
	pablo.LastName = "Zenteno"
	pablo.Age = 32
	pablo.DatosPersonales = &DatosPersonales{
		Direccion: "Pasaje ababajanbj",
	}

	fmt.Println(pablo)
	fmt.Println(pablo.NombreCompleto())
	fmt.Println(pablo.NombreCompletoConEdad())

	felipe := &Persona{
		ID:       "2",
		Name:     "Felipe",
		LastName: "Chacon",
		Age:      34,
		DatosPersonales: &DatosPersonales{
			Direccion: "Direccion",
		},
	}

	//fmt.Println(felipe.NombreCompletoConEdad())

	fmt.Println(&felipe)
	fmt.Println(*felipe.DatosPersonales)

	var numero int = 20
	fmt.Println(numero)
	fmt.Println(&numero)

	listaA := make([]string, 0)
	listaA = append(listaA, "a", "b")

	listaB := make([]string, 0)
	listaB = append(listaB, "c", "d")
	fmt.Println("Lista A antes", listaA)
	listaA = append(listaA, listaB...)
	fmt.Println("Lista A despues", listaA)
	fmt.Println("Lista B", listaB)

}

type Persona struct {
	ID              string
	Name            string
	LastName        string
	Age             int
	DatosPersonales *DatosPersonales
}

func (p *Persona) NombreCompleto() string {
	return fmt.Sprintf("%s %s", p.Name, p.LastName)
}
func (p *Persona) NombreCompletoConEdad() string {
	return fmt.Sprintf("%s %s %d", p.Name, p.LastName, p.Age)
}

type DatosPersonales struct {
	Direccion string
}
