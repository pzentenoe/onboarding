FROM golang:alpine as builder
#RUN apk --no-cache add git dep ca-certificates
#RUN apk --no-cache add tzdata

ENV GOBIN=$GOPATH/bin
ENV GO111MODULE="on"

RUN mkdir -p $GOPATH/src/gitlab.com/pzentenoe/onboarding
WORKDIR $GOPATH/src/gitlab.com/pzentenoe/onboarding

COPY go.mod .
COPY vendor vendor/
COPY app/ app/

RUN CGO_ENABLED=0 GOOS=linux go build -mod=vendor -a -installsuffix cgo -ldflags '-extldflags "-static"' -o $GOBIN/main ./app/main.go


# Runtime image with scratch container
FROM scratch
ARG VERSION
ENV APP_VERSION=$VERSION

#COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /go/bin/ /app/
#COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo

#ENV TZ America/Santiago

ENTRYPOINT ["/app/main"]